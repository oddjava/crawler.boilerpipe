package boilerpipeCrawler;

import java.io.IOException;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;



public class KeywordAutomation {

	public static void main(String[] args) throws Exception 
	{
		Node node1=new Node();
		KeywordAutomation keyauto=new KeywordAutomation();
		String url="https://blog.shareaholic.com/hard-marketing-truths/";			
		try
		{
		node1=keyauto.chechMetaData(url);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("Node:::"+node1);
	}
	
	//This method will take input as url and it will return Node object
	//in Node object it will give us
	//keywordTag,descriptionTag,
	//keywordContent,descriptionContents
	
	public Node chechMetaData(String url)throws IOException,Exception
	{
		String descriptionTag="",keywordTag="",paragraph="";
		Node node=new Node();
		String keywordContent="",descriptionContents="";
		Document doc = Jsoup.connect(url).timeout(1000000).get();
		node.title=doc.title();
	//	System.out.println(doc);
		for(Element meta : doc.select("meta"))
		{
			if(meta.attr("name").toLowerCase().contains("keyword"))
			{
				//System.out.println("true");
				keywordTag= meta.attr("name");
				node.keywordTag.add(keywordTag);
				keywordContent=meta.attr("content");
				node.keywordContent=node.keywordContent+","+keywordContent;
				node.keywordContent=node.keywordContent.substring(1);
			}
			
			if(meta.attr("name").toLowerCase().contains("description"))
			{
				descriptionTag=meta.attr("name");
				node.descriptionTag.add(descriptionTag);
				descriptionContents=meta.attr("content");			
				node.descriptionContents=node.descriptionContents+"\t"+descriptionContents;
					
			}			
				
		}		
		
		return node;
	}
	
	
}