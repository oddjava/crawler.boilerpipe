package boilerpipeCrawler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import boilerpipeCrawler.GetData;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import mongoDBConnection.GetConnection;
import removeSpecialChar.RemoveSpecialChar;

public class Crawler {

	private static final int MYTHREADS = 10;

	static DBCollection crawldata = GetConnection.connect("crawldata");
	static DBCollection sampleCrawldata = GetConnection.connect("sampleCrawldata");
	static GetData getData = new GetData();

	public void crawlSeeds(ArrayList<String> seeds) {
		ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
		for (int i = 0; i < seeds.size(); i++) {
			System.out.println("Seed is " + seeds.get(i));
			String url = seeds.get(i);

			Runnable worker = new MyRunnable(url);
			// executor.submit(worker);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}
		System.out.println("\nFinished all threads");

	}

	public static void main(String args[]) throws Exception {

		ArrayList<String> list = new ArrayList<>();
		ArrayList<String> crawlList = new ArrayList<>();

		long startTime = System.currentTimeMillis();
		BasicDBObject query = new BasicDBObject("status", "false");

		// List<BasicDBObject> obj = new ArrayList<BasicDBObject>();

		// obj.add(new BasicDBObject("boilerpipeStatus", "false"));

		// query.put("$and", obj);
		DBCursor cursor = crawldata.find(query);
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		int cursorSize = cursor.size();
		int cursorLimit = 0;
		int batch = 0;

		if (cursorSize >= 100) {
			System.out.println("Adding " + cursorSize + " data in list..... Please wait for some time");
			while (cursor.hasNext()) {
				DBObject object = cursor.next();
				// System.out.println(object);
				String link = object.get("link").toString();
				String boilerpipeStatus = object.get("boilerpipeStatus").toString();

				if (boilerpipeStatus.contains("false"))
					crawlList.add(link);
				/*
				 * if (list.size() == 100) { System.out.println(
				 * "List is 100 and calling threads"); new
				 * Crawler().crawlSeeds(list); System.out.println(
				 * "Now list is clear"); list.clear(); } else { list.add(link);
				 * }
				 */
			}
			cursor.close();
		}

		if (crawlList.size() < 100) {
			System.out.println("Atleast 100 records are required to start this crawler");
			System.out.println("Waiting for another 2 minutes to see if 100 records are present");
			System.gc();
			Thread.sleep(120000); // 2 minutes in milliseconds
			main(args);

		} else {
			while (cursorLimit != crawlList.size()) {
				if (batch < 100) {
					list.add(crawlList.get(cursorLimit));
					cursorLimit++;
					batch++;
				} else {
					System.out.println("List is 100 and calling threads");
					new Crawler().crawlSeeds(list);
					System.out.println("Now list is clear");
					list.clear();
					batch = 0;
				}
			}
		}

		if (list.size() > 0) {
			System.out.println("List is " + batch + " and calling threads");
			new Crawler().crawlSeeds(list);
			System.out.println("Final list is clear");
			list.clear();
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Done with all the execution");
		long time = endTime - startTime;
		System.out.println("Program took " + time + " to execute");
		System.out.println("After 2 minutes program will restart");
		System.gc();
		Thread.sleep(120000); // 2 minutes in milliseconds
		main(args);

	}

	public static class MyRunnable implements Runnable {
		private final String urlToProcess;
		Document doc;

		MyRunnable(String urlToProcess) {
			this.urlToProcess = urlToProcess;
		}

		@Override
		public void run() {

			try {
				URL url = new URL(urlToProcess);
				String textToProcess, email, phone;
				textToProcess = ArticleExtractor.INSTANCE.getText(url);
				textToProcess = textToProcess.trim();
				if (textToProcess.isEmpty()) {
					textToProcess = "Boilerpipe output empty";
				}
				email = getData.getEmailId(textToProcess);
				phone = getData.getPhone(textToProcess);

				BasicDBObject searchQuery = new BasicDBObject("link", urlToProcess);

				BasicDBObject updateFields = new BasicDBObject();
				updateFields.append("mainContent", textToProcess);
				updateFields.append("companyEmail", email);
				updateFields.append("companyNumber", phone);
				updateFields.append("boilerpipeStatus", "true");

				BasicDBObject setQuery = new BasicDBObject();
				setQuery.append("$set", updateFields);

				sampleCrawldata.update(searchQuery, setQuery);
				crawldata.update(searchQuery, setQuery);
				System.out.println("Done with " + urlToProcess);
				// }
				// }

			} catch (BoilerpipeProcessingException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				System.out.println(e.getMessage());
				// System.out.println("Exception in boiler pipe");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("List of text To Process:"+textsToProcess);
		}

	}

}
