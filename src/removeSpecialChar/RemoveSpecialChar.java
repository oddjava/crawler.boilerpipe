package removeSpecialChar;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoveSpecialChar {
	public static String removeSpecialChars(String title) {
		String refinedTitle = null;
		Pattern p = Pattern.compile("[^0-9A-Za-z- ]+");
		Matcher m = p.matcher(title);
		if (m.find()) {
			refinedTitle = m.replaceAll(" ");
			return refinedTitle;
		} else
			return title;
	}
	
	public static void main(String[] args) {
		System.out.println(removeSpecialChars("This employer requests that only candidates in United States apply to this job.You appear to be located in United Kingdom, not United States.Please be aware of this if you choose to apply for this job.The HOTH is seeking freelance writers interested in gaining real world experience developing content for our clients within many different industries.Freelancers will partner with our digital marketing team who will assign writing topics and deadlines. If you are business savvy, have a knack for researching niche topics and possess journalistic writing skills, we would like to chat with you!Top reasons to partner with The HOTH:Fill your portfolio with a variety of professional writing samplesGain experience writing for multiple industriesOpportunity to ghost write articles published in industry blogs andpublicationsEndless opportunity for ongoing workCompetitive compensation per writing projectTop things we look for in our freelancersExperience working under tight deadlinesAbility to quickly grasp complex concepts and make them easily understandableThorough researching skillsAbility to gather additional information for blog and article writingExcellent writing and grammar skillsStrong verbal communication skillsUnderstanding of inbound marketing, content marketing, and SEO a plus!As a freelancer you will:Develop content including (but not limited to): blog posts & industry articlesPerform in-depth research for assigned topics within many different types of industriesParticipate in occasional HOTH content marketing meetings when requestedYour email address"));
	}
}